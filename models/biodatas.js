const mongoose = require("mongoose");

const biodatasSchema = new mongoose.Schema(
  {
    gender: {
      type: String,
      required: true,
      enum: ["male", "female"],
    },
    name: {
      type: String,
      required: false,
    },
    birth_date: {
      type: String,
      required: false,
    },
    address: {
      type: String,
      required: false,
    },
    contact_number: {
      type: String,
      required: false,
    },
    patien_height: {
      type: Number,
      required: false,
    },
    patien_weight: {
      type: Number,
      required: false,
    },
    email: {
      type: String,
      required: false,
    },
    reason_see_doctor: {
      type: String,
      required: false,
    },
    drug_allergies: {
      type: String,
      required: false,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    toJSON: { getters: true },
  }
);

// function getPoster(poster) {
//   if (!poster || poster.includes("https") || poster.includes("http")) {
//     return poster;
//   }

//   return `/images/poster/${poster}`;
// }

module.exports = mongoose.model("biodatas", biodatasSchema);
