const mongoose = require("mongoose");

const printsSchema = new mongoose.Schema(
  {
    print_queue: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    toJSON: { getters: true },
  }
);

// function getPoster(poster) {
//   if (!poster || poster.includes("https") || poster.includes("http")) {
//     return poster;
//   }

//   return `/images/poster/${poster}`;
// }

module.exports = mongoose.model("prints", printsSchema);
