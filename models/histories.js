const mongoose = require("mongoose");

const historiesSchema = new mongoose.Schema(
  {
    biodatas_id: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
    },
    date_visiting_hospital: {
      type: Date,
    },
    info_control: {
      type: Boolean,
      required: true,
    },
    control_schedule: {
      type: String,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    toJSON: { getters: true },
  }
);

// function getPoster(poster) {
//   if (!poster || poster.includes("https") || poster.includes("http")) {
//     return poster;
//   }

//   return `/images/poster/${poster}`;
// }

module.exports = mongoose.model("prints", printsSchema);
