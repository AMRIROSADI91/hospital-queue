// Amri's Code
const express = require("express");

// Print validator
const {
  queryPrintValidator,
  getDetailValidator,
  printValidator,
} = require("../middlewares/validators/prints");

const {
  queryHistoryValidator,
  historyValidator,
} = require("../middlewares/validators/histories");

// Import controller
const {
  createPrint,
  getAllPrint,
  getPrintById,
  updatePrint,
  deletePrint,
} = require("../controllers/prints");

const { updateHistory } = require("../controllers/histories");

// Make router
const router = express.Router();

// Make some routes
router.post("/", printValidator, createPrint);
router.get("/", queryPrintValidator, getAllPrint);
router.get("/:id", getDetailValidator, queryPrintValidator, getPrintById);
router.put("/edit/:id", printValidator, getDetailValidator, updatePrint);
router.put(
  "/edit/:id/record",
  historyValidator,
  getDetailValidator,
  updateHistory
);
router.delete("/:id", getDetailValidator, deletePrint);

// Exports
module.exports = router;
