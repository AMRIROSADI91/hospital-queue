// Amri's Code
const express = require("express");

// Print validator
const {
  queryBiodataValidator,
  getDetailValidator,
  biodataValidator,
} = require("../middlewares/validators/biodatas");

const {
  queryHistoryValidator,
  historyValidator,
} = require("../middlewares/validators/histories");

// Import controller
const {
  createBiodata,
  getAllBiodata,
  getBiodataById,
  updateBiodata,
  deleteBiodata,
} = require("../controllers/biodatas");

const { updateHistory } = require("../controllers/histories");

// Make router
const router = express.Router();

// Make some routes
router.post("/", biodataValidator, createBiodata);
router.get("/", queryBiodataValidator, getAllBiodata);
router.get("/:id", getDetailValidator, queryBiodataValidator, getBiodataById);
router.put("/edit/:id", biodataValidator, getDetailValidator, updateBiodata);
router.put(
  "/edit/:id/record",
  historyValidator,
  getDetailValidator,
  updateHistory
);
router.delete("/:id", getDetailValidator, deleteBiodata);

// Exports
module.exports = router;
