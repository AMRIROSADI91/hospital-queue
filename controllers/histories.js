// Amri's Code
const express = require("express");

// histories validator
const {
  queryHistoryValidator,
  getDetailValidator,
  historyValidator,
} = require("../middlewares/validators/histories");

// Import controller
const {
  getAllHistory,
  getHistoryById,
  updateHistory,
} = require("../controllers/histories");

// item validator
const {
  queryItemValidator,
  itemValidator,
} = require("../middlewares/validators/items");

// Import controller
const { getAllItem, getItemById, updateItem } = require("../controllers/items");

// Make router
const router = express.Router();

// Make some routes
router.get("/", queryHistoryValidator, getAllHistory);
router.get("/:id", getDetailValidator, queryHistoryValidator, getHistoryById);
// router.put(
//   "/edit/:id/record",
//   historyValidator,
//   getDetailValidator,
//   updateHistory
// );

// Exports
module.exports = router;
