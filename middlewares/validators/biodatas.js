// Amri's Code
const { prints, biodatas } = require("../../models");
const validator = require("validator");
const mongoose = require("mongoose");

exports.queryBiodataValidator = async (req, res, next) => {
  try {
    const errorMessages = [];

    if (req.query.limit) {
      if (!validator.isInt(req.query.limit)) {
        errorMessages.push("Please enter proper number for limit query");
      }
    }

    if (req.query.page) {
      if (!validator.isInt(req.query.page)) {
        errorMessages.push("Please enter proper number for page query");
      }
    }

    if (errorMessages.length > 0) {
      return next({ statusCode: 400, messages: errorMessages });
    }

    next();
  } catch (error) {
    next(error);
  }
};

exports.getDetailValidator = async (req, res, next) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return next({ message: "id is not valid", statusCode: 400 });
    }

    next();
  } catch (error) {
    /* istanbul ignore next */
    next(error);
  }
};

exports.biodataValidator = async (req, res, next) => {
  try {
    /* Validate the user input */
    const errorMessages = [];

    const gender = ["male", "female"];
    if (!gender.includes(req.body.gender)) {
      errorMessages.push("Gender type is not valid. Input male / female.");
    }

    if ((req.body.name && req.body.name <= 0) || !req.body.name) {
      errorMessages.push("Name is required");
    }
    if (validator.isEmpty(req.body.user_birthday)) {
      errorMessages.push("Please input your birthday");
    }

    if (!validator.isDate(req.body.user_birthday)) {
      errorMessages.push("Birthday is not valid");
    }

    if ((req.body.address && req.body.address <= 0) || !req.body.address) {
      errorMessages.push("Address is required");
    }

    if (
      req.body.contact_number &&
      !validator.isLength(req.body.contact_number, { min: 10, max: 12 })
    ) {
      {
        errorMessages.push("Contact number is not valid");
      }
    }

    if (req.body.patien_height && !validator.isInt(req.body.patien_height)) {
      errorMessages.push("Invalid patien height format! Please insert numeric");
    }

    if (req.body.patien_weight && !validator.isInt(req.body.patien_weight)) {
      errorMessages.push("Invalid patien weight format! Please insert numeric");
    }

    if (req.body.email && !validator.isEmail(req.body.email)) {
      errorMessages.push("Email account is not valid");
    }

    if (
      (req.body.reason_see_doctor && req.body.reason_see_doctor <= 0) ||
      !req.body.reason_see_doctor
    ) {
      errorMessages.push("Reason to see doctor is required");
    }

    if (
      (req.body.drug_allergies && req.body.drug_allergies <= 0) ||
      !req.body.drug_allergies
    ) {
      errorMessages.push("Drug allergies is required");
    }

    if (errorMessages.length > 0) {
      return next({ statusCode: 400, messages: errorMessages });
    }

    next();
  } catch (error) {
    next(error);
  }
};
