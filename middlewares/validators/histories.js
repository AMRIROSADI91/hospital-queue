// Amri's Code
const { biodatas, histories } = require("../../models");
const validator = require("validator");
const mongoose = require("mongoose");

exports.queryHistoryValidator = async (req, res, next) => {
  try {
    const errorMessages = [];

    if (req.query.limit) {
      if (!validator.isInt(req.query.limit)) {
        errorMessages.push("Please enter proper number for limit query");
      }
    }

    if (req.query.page) {
      if (!validator.isInt(req.query.page)) {
        errorMessages.push("Please enter proper number for page query");
      }
    }

    if (errorMessages.length > 0) {
      return next({ statusCode: 400, messages: errorMessages });
    }

    next();
  } catch (error) {
    next(error);
  }
};

exports.getDetailValidator = async (req, res, next) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return next({ message: "id is not valid", statusCode: 400 });
    }

    next();
  } catch (error) {
    /* istanbul ignore next */
    next(error);
  }
};

exports.historyValidator = async (req, res, next) => {
  try {
    /* Validate the user input */
    const errorMessages = [];

    if (
      req.body.date_visiting_hospital &&
      !validator.isDate(req.body.date_visiting_hospital)
    ) {
      errorMessages.push(
        "Invalid date. Please input date in a proper format (YYYY-MM-DD"
      );
    }

    if (errorMessages.length > 0) {
      return next({ statusCode: 400, messages: errorMessages });
    }

    next();
  } catch (error) {
    next(error);
  }
};
